 # GrapheneOS-by-SuperITMan - Patches

This repository contains multiple patches to apply to GrapheneOS-by-SuperITMan build.
All of them must be applied from the ROOT folder!

## Android Auto

This patch has been written by SolidHal and is available on GitHub: https://github.com/SolidHal/Build-GrapheneOS-Microg-AndroidAuto-Oriole-Raven

```shell
git apply ./patches/0001-Allow-required-Android-Auto-permissions.patch
```

## Default apps

This patch changes the default apps added in the custom build.

```shell
git apply ./patches/default-apps/default-apps.patch
```

## Updater app

This patch changes the release URL to use SuperITMan server.

```shell
git apply ./patches/apps-updater/updater-release-url.patch
```
